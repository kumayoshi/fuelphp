<?php

class Controller_Admin extends Controller
{
	//action_loginっていう感じに設定するとfuel/admin/loginっていうurlで表示できる。
	// fuel/admin/loginっていうのはfuel/app/views/admin/loginが読まれるがurlはfuel/admin/loginになる。
	public function action_login()
	{
		if(Input::post('id')=='admin' && Input::post('password')=='pass'){
			Session::set('admin_login',true);
			echo "ログインOK!";
			// Response::redirectで指定したパスに飛ぶ
			// この場合もさっきと同じようにaction_なんちゃらで指定できる

		}
		// セッションをちゃんと取得しているならadmin/listに飛ぶ
		if (Session::get('admin_login') == true) {
			Response::redirect('admin/list');
		}
		// Response::forgeっていうのはwordpressでいう固定ページのデフォルトテンプレート
		return Response::forge(View::forge('admin/login'));
	}


	public function action_list()
	{
		if(Session::get('admin_login') != true)
		{
			Response::redirect('admin/login');
		}

		// データをロード
		$images = Model_Image::find('all');
		$data = array('images' => $images);

		return Response::forge(View::forge('admin/list',$data));
	}


	public function action_logout()
	{
		Session::delete('admin_login');
		return Response::redirect('admin/login');
	}


	public function action_upload()
	{
		if(Session::get('admin_login') != true)
		{
			Response::redirect('admin/login');
		}

		if (Input::file('image')){
			$image = Input::file('image');
			move_uploaded_file($image['tmp_name'],'assets/img/'.$image['name']);

			$model_Image = new Model_Image();
			$model_Image->file_name = $image['name'];
			$model_Image->info = '';
			$model_Image->votes = 0;
			$model_Image->save();

		}

		return Response::forge(View::forge('admin/upload'));
	}
}

// セッションを構う場合
// core/session.phpをappのconfigに持ってくる
// 'driver'			=> 'file'  で  'file'				=> array('cookie_name'		=> 'votesystem'
// に変える
