<?php

class Controller_Bye extends Controller
{
	public function action_index()
	{
		$date = array(
			'title' => '別れの挨拶',
			'message' => 'じゃあな',
		);
		$view = View::forge('bye/index',$date);
		return response::forge($view);
	}
}
