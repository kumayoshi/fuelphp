<?php

class Controller_Vote extends Controller{
	public function action_login()
	{
		if(Input::post('id') != ''){
			if(Auth::login(Input::post('id'),Input::post('password')))
			{
				Response::redirect('vote/list');
			}
		}

		return Response::forge(View::forge('vote/login'));
	}
	public function action_logout()
	{
		Auth::logout();
		Response::redirect('vote/login');
	}
	public function action_list()
	{
		if(! Auth::check())
		{
			Rsponse::redirect('vote/login');
		}
		$images = Model_Image::find('all');
		$data = array('images' => $images);
		return Response::forge(View::forge('vote/list',$data));
	}
}
