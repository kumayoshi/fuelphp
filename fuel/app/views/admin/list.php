<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>写真投稿</title>
  <style media="screen">
    main{
      box-sizing: border-box;
      padding: 30px;
    }
    li{
      list-style: none;
      width: 40%;
    }
    img{
      width: 100%;
      height: 100%;
    }
  </style>
</head>
<body>
  <main>
      <ul>
        <?php foreach($images as $img): ?>
          <li>
            <?php echo Asset::img($img['file_name']); ?>
            <p class="votes"><?php echo $img['votes']; ?></p>
          </li>
        <?php endforeach; ?>
      </ul>
      <?php echo Html::anchor('admin/logout','ログアウト') ?>
      <br>
      <?php echo Html::anchor('admin/upload','アップロード');  ?>
  </main>
</body>
</html>
