<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>写真投稿</title>
  <style media="screen">
    main{
      box-sizing: border-box;
      padding: 30px;
    }

  </style>
</head>
<body>
  <main>
    <p>
      アップロードに移動しました。
    </p>
    <?php echo Html::anchor('admin/logout','ログアウト'); ?>
    <?php echo Html::anchor('admin/list','一覧画面'); ?>

    <?php echo Form::open(array('action' => 'admin/upload','method' => 'post','enctype' => 'multipart/form-data')); ?>
    <?php echo Form::file('image'); ?>


    <?php echo Form::submit('upload','アップロード'); ?>
    <?php echo Form::close(); ?>
  </main>
</body>
</html>
