<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <script
    src="https://code.jquery.com/jquery-2.2.4.js"
    integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
    crossorigin="anonymous"></script>
    <script type="text/javascript">
      $(function(){
        $("input.vote").on("click",function(){
          var id = $(this).data('id');
          console.log(id);
          $.ajax({
            url:"<?php echo Uri::create('api/vote.json'); ?>",
            type:"POST",
            data:{id:id},
            dataType:"json",
          }).done(function(data){
            alert(data.message);
            location.reload();
          }).fail(function(data){
            // alert("失敗した");
          });
        });
      });
    </script>
  <title>写真投稿</title>
  <style media="screen">
    main{
      box-sizing: border-box;
      padding: 30px;
    }
    li{
      list-style: none;
      width: 40%;
    }
    img{
      width: 100%;
      height: 100%;
    }
  </style>
</head>
<body>
  <main>
      <ul>
        <?php foreach($images as $img): ?>
          <li>
            <?php echo Asset::img($img['file_name']); ?>
            <p class="votes"><?php echo $img['votes']; ?></p>
            <input type="button" class="vote" data-id="<?php echo $img['id']; ?>" value="投票する">
          </li>
        <?php endforeach; ?>
        <li>リスト</li>
        <li>リスト</li>
        <li>リスト</li>
        <li>リスト</li>
        <li>リスト</li>
      </ul>
      <?php echo Html::anchor('vote/logout','ログアウト') ?>
      <br>
  </main>
</body>
</html>
